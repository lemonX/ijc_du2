//////////////////////////////////
// file:     fold2.cc
// project:  IJC - DU2
// compiler: GCC 4.8.1
// created:  2014-04-14
// finish:   2014-04-26
//
// author:  Pavel Mencner
//          University of technology Brno - FIT
//
// Description: ze zadaného vstupního souboru čte text a formátuje ho
// na zadanou šířku. Verze C++
//////////////////////////////////

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>

//using namespace std;

typedef struct {
  unsigned long line_width; //width of output line
  bool cut_longer; //cutting word longer than width
  std::istream *inputs;
} Params;

//ANO hovadismus///////
std::ifstream myfile;//
///////////////////////

/**
 * Processing of parameters from command line
 * @param argc Number of parameters
 * @param argv Strings of parameters
 * @return     Struct with parameters
 */
Params get_params(int argc, char **argv)
{
  Params params = {80, false, &std::cin};
  std::string param;

  for(int i = 1; i < argc; i++)
  {//processing od params
    param = argv[i];

    if(param.compare("-w") == 0)
    {
      if(argc > i+1)
      {
        i++;
        std::istringstream ss(argv[i]);
	
	if(!(ss >> params.line_width))
        {
	  std::cerr << "Error, numeric argument expected after '-w'";
          exit(1);
	}
      }
      else
      {
        std::cerr << "Error, numeric argument expected after '-w'";
        exit(1);
      }
    }
    else if(param.compare("-c") == 0)
    {
      params.cut_longer = true;
    }
    else
    {
      if(argc == i+1)
      {//if this is last param => file name
	myfile.open(argv[i]);
	params.inputs = &myfile;

	if(!myfile.is_open())
        {
          std::cerr << "Error, can't read a file";
          exit(1);
        }
      }
      else
      {//some shit found on cmd
        std::cerr << "Error, Wrong argument found";
        exit(1);
      }
    }
  }

  return params;
}

int format_text(Params *params)
{
  using namespace std;
  string line;
  istringstream iss;
  string word;
  unsigned long line_chars = 0;
  unsigned long word_len = 0;
  bool is_word = true;
  bool first = true;
  bool reported_line_overflow = false;

  while (getline(*params->inputs, line))
  {
      iss.str(line);
      iss.clear();
      
      if(is_word == false)
      {
        if(first == false)
        {
	  cout << endl;
	  first = true;
	}
	cout << endl;
	line_chars = 0;
      }
      is_word = false;

      while(iss >> word)
      {
        is_word = true;
	first = false;
	word_len = word.length();
	
	if (line_chars + word_len < params->line_width)
        {
	  if(line_chars > 0)
	  {
	    cout << " ";
	    line_chars++;
	  }
	  cout << word;
	  line_chars += word_len;
	}
	else
	{
	  cout << endl;
	  line_chars = 0;
	  
	  if(params->cut_longer == true && word_len > params->line_width)
	  {
	    if(reported_line_overflow == false)
	    {
	      reported_line_overflow = true;
	      cerr << "Warning, some words are longer than line\n";
	    }
	    
	    word = word.substr(0, params->line_width);
	    cout << word << endl;
	    first = true;
	  }
	  else
	  {
	    cout << word;
	    line_chars += word_len;
	  }
	}
	
      } 
  }

  return 0;
}

int main(int argc, char **argv)
{
  using namespace std;
  Params params = get_params(argc, argv);
  format_text(&params);
  return 0;
}
