//////////////////////////////////
// file:     wordcount.c
// project:  IJC - DU2
// compiler: GCC 4.8.1
// created:  2014-04-17
// finish:   2014-04-18
//
// author:  Pavel Mencner
//          University of technology Brno - FIT
//
// Description: Count number of words read from stdin, word mean everything
// except white spaces (space, enter, tab...). Optimalized for huge data
// input (with MBs files its faster than C++ asociative array
//
//////////////////////////////////

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "htable.h"
#include "io.h"

#define MAX 127

void print_listitem(const char *key, unsigned long value)
{
  printf("%s\t%lu\n", key, value);
}

int main(void)
{
  htab_t *table = NULL;
  htab_listitem_t *word = NULL;
  char s[MAX] = {0}; //buffer
  int len = 0;
  bool overflow_warning = false;

  /**
   * Experimentalne bylo zjisteno ze pri velke hashovaci tabulce
   * sice rostou linearne pametove naroky na aplikaci, avsak drasticky
   * klesa narocnost casova (radove) pri velkych datovych vstupech
   * (stovky tisic slov), ktere vsak pro podobnou aplikaci budou
   * typickym uplatnenim
   * 
   * zvolena velikost 50000
   *   -neprekroci UINT_MAX ani pri 16b int
   *   -hashovaci tabulka potrebuje prijatelnych 400KB pameti
   */
  if((table = htab_init(50000)) == NULL)
    return 1;
  
  while((len = fgetw(s, MAX, stdin)) != EOF)
  {
    if(overflow_warning == false && len >= MAX)
    {
      fprintf(stderr, "Warning, some too long word cut\n");
      overflow_warning = true;
    }

    if((word = htab_lookup(table, s)) == NULL)
      return 1;

    word->data++;

  }
  
  htab_foreach(table, print_listitem);
  
  htab_free(table);
  
  return 0;
}

