//////////////////////////////////
// file:     htab_remove.c
// project:  IJC - DU2
// compiler: GCC 4.8.1
// created:  2014-04-17
// finish:   2014-04-18
//
// author:  Pavel Mencner
//          University of technology Brno - FIT
//
// Description: Find and remove one specific item from list array
// 
//////////////////////////////////

#include <string.h>
#include "htable.h"

/**
 * @arg t pointer to struct with hash table
 * @arg key pointer to string
 * @return '0' if delete item, '1' if item dont exist
 */
int htab_remove(htab_t *t, const char *key)
{
  unsigned hindex = hash_function(key, t->htab_size); //index to hash table
  htab_listitem_t *item_ptr = t->ptr[hindex]; //pointer to list
  htab_listitem_t **previous_ptr = &(t->ptr[hindex]); //pointer to list

  while(item_ptr != NULL)
  {
    if(strcmp(item_ptr->key, key) == 0)
    { //item found, delete
      *previous_ptr = item_ptr->next;
      free(item_ptr);
      return 0;
    }

    previous_ptr = &(item_ptr->next);
    item_ptr = item_ptr->next;
  }

  return 1;
}
