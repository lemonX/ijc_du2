//////////////////////////////////
// file:     htable.h
// project:  IJC - DU2
// compiler: GCC 4.8.1
// created:  2014-04-14
// finish:   2014-04-18
//
// author:  Pavel Mencner
//          University of technology Brno - FIT
//
// Description: Interface for hash functions
// 
//////////////////////////////////

#include <stdlib.h>

/**
 * Types for structures defined below
 */
typedef struct htab htab_t;
typedef struct htab_listitem htab_listitem_t;

/**
 * Structure for hash table
 * htab_size - size of hash table
 * htab_listitem - (flexible) array of pointers to data
 */
struct htab {
  unsigned htab_size;
  struct htab_listitem *ptr[];
};

/**
 * Structure for item (word)
 * data - counter of words instance
 * next - pointer to next item
 * key - counting word (flexible member)
 */
struct htab_listitem {
  unsigned long data;
  struct htab_listitem *next;
  char key[];
};

/**
 * Prototypes
 */
unsigned int hash_function(const char *str, unsigned htab_size);
htab_t *htab_init(unsigned size);
unsigned int hash_function(const char *str, unsigned htab_size);
struct htab_listitem *htab_lookup(htab_t *t, const char *key);
int htab_remove(htab_t *t, const char *key);
void htab_clear(htab_t *t);
void htab_free(htab_t *t);
void htab_statistics(htab_t *t);
void htab_foreach(htab_t *t, void (*function)(const char *key, unsigned long value));
