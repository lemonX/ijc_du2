all: fold fold2 wordcount wordcount-dynamic

CC=gcc
CFLAGS=-fPIC -Wall -Wextra -pedantic -std=c99

fold: fold.c
	$(CC) $(CFLAGS) fold.c -o fold

fold2: fold2.cc
	g++ fold2.cc -Wall -Wextra -pedantic -std=c++11 -o fold2

hash_function.o: hash_function.c htable.h
	$(CC) $(CFLAGS) -c hash_function.c

io.o: io.c
	$(CC) $(CFLAGS) -c io.c

htab_init.o: htab_init.c htable.h
	$(CC) $(CFLAGS) -c htab_init.c

htab_lookup.o: htab_lookup.c htable.h
	$(CC) $(CFLAGS) -c htab_lookup.c

htab_remove.o: htab_remove.c htable.h
	$(CC) $(CFLAGS) -c htab_remove.c

htab_clear.o: htab_clear.c htable.h
	$(CC) $(CFLAGS) -c htab_clear.c

htab_free.o: htab_free.c htable.h
	$(CC) $(CFLAGS) -c htab_free.c

htab_statistics.o: htab_statistics.c htable.h
	$(CC) $(CFLAGS) -c htab_statistics.c

htab_foreach.o: htab_foreach.c htable.h
	$(CC) $(CFLAGS) -c htab_foreach.c

wordcount.o: wordcount.c htable.h
	$(CC) $(CFLAGS) -c wordcount.c

libhtable.a: htab_init.o hash_function.o htab_lookup.o htab_remove.o htab_clear.o htab_free.o htab_statistics.o htab_foreach.o
	ar rcs libhtable.a htab_init.o hash_function.o htab_lookup.o htab_remove.o htab_clear.o htab_free.o htab_statistics.o htab_foreach.o
	ranlib libhtable.a

libhtable.so: htab_init.o hash_function.o htab_lookup.o htab_remove.o htab_clear.o htab_free.o htab_statistics.o htab_foreach.o
	$(CC) -shared -fPIC htab_init.o hash_function.o htab_lookup.o htab_remove.o htab_clear.o htab_free.o htab_statistics.o htab_foreach.o -o libhtable.so

wordcount: wordcount.o libhtable.a io.o
	 $(CC) wordcount.o io.o -L. -lhtable -o wordcount

wordcount-dynamic: wordcount.o libhtable.so io.o
	$(CC) wordcount.o io.o -L. -lhtable -o wordcount-dynamic

