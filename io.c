//////////////////////////////////
// file:     io.c
// project:  IJC - DU2
// compiler: GCC 4.8.1
// created:  2014-04-16
// finish:   2014-04-16
//
// author:  Pavel Mencner
//          University of technology Brno - FIT
//
// Description: Function reads 1 word from file
// 
//////////////////////////////////

#include <ctype.h>
#include <stdio.h>
#include <stdbool.h>
#include "io.h"

/**
 * Function reads 1 word from file f to string s of max len max
 * @arg s pointer to array of char to output a read word
 * @arg max max length of word (length of array s)
 * @arg f pointer to file from which read a words
 * @return length of read word (even more than max) or EOF
 */
int fgetw(char *s, int max, FILE *f)
{
  int c;
  bool word_start = false;
  int counter = 0;
  
  s[max-1] = '\0'; //security stopper

  while((c = getc(f)))
  {
    if(word_start == false && isspace(c)) //jump over white spaces
      continue;
      
    if(word_start == false && c == EOF) //EOF return
      return EOF;

    if(word_start == true && isspace(c)) //end of word
    {
      if(counter < max-1)
        s[counter] = '\0';
      return counter;
    }

    if(word_start == false)
      word_start = true;

    if(counter < max-1)
      s[counter] = c;
    
    counter++;

  }
  
  return EOF;
}
