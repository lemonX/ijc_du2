//////////////////////////////////
// file:     htab_init.c
// project:  IJC - DU2
// compiler: GCC 4.8.1
// created:  2014-04-11
// finish:   2014-04-18
//
// author:  Pavel Mencner
//          University of technology Brno - FIT
//
// Description: Initialization of hash table
// 
//////////////////////////////////

#include "htable.h"

htab_t *htab_init(unsigned size)
{
  htab_t *hashtable = (htab_t *) malloc(sizeof(htab_t) + size * sizeof(htab_listitem_t *));
  
  if(hashtable != NULL)
  {
    hashtable->htab_size = size;
    
    for(unsigned i = 0; i < size; i++)
      hashtable->ptr[i] = NULL;
  }
  
  return hashtable;
}
