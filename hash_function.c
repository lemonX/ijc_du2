//////////////////////////////////
// file:     hash_fuction.c
// project:  IJC - DU2
// compiler: GCC 4.8.1
// created:  2014-04-14
// finish:   2014-04-18
//
// author:  Pavel Mencner
//          University of technology Brno - FIT
//
// Description: ze zadaného vstupního souboru čte text a formátuje ho
// na zadanou šířku.
//////////////////////////////////

#include "htable.h"

unsigned int hash_function(const char *str, unsigned htab_size)
{
  unsigned int h=0;
  const unsigned char *p;
  
  for(p=(const unsigned char*)str; *p!='\0'; p++)
    h = 65599*h + *p;
  
  return h % htab_size;
}
                        
