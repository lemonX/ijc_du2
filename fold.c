//////////////////////////////////
// file:     fold.c
// project:  IJC - DU2
// compiler: GCC 4.8.1
// created:  2014-04-11
// finish:   2014-04-13
//
// author:  Pavel Mencner
//          University of technology Brno - FIT
//
// Description: ze zadaného vstupního souboru čte text a formátuje ho
// na zadanou šířku. Verze C
//////////////////////////////////

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

typedef struct {
  unsigned long line_width; //width of output line
  bool cut_longer; //cutting word longer than width
  FILE *file; //pointer to file with input data
  int err_code;
} Params;

/**
 * Processing of parameters from command line
 * @param argc Number of parameters
 * @param argv Strings of parameters
 * @return     Struct with parameters
 */
Params get_params(int argc, char **argv)
{
  Params params = {
    .line_width = 80,
    .cut_longer = false,
    .file = stdin,
    .err_code = 0
  };

  for(int i = 1; i < argc; i++)
  {//processing od params
    if(strcmp(argv[i], "-w") == 0)
    {
      if(argc > i+1)
      {
        i++;
        char *foo = NULL;
        params.line_width = strtol(argv[i], &foo, 10);
        if(foo == argv[i] || *foo != '\0')
        {
          fprintf(stderr, "Error, numeric argument expected after '-w'\n");
          exit(1);
        }
      }
      else
      {
        fprintf(stderr, "Error, numeric argument expected after '-w'\n");
        exit(1);
      }
    }
    else if(strcmp(argv[i], "-c") == 0)
    {
      params.cut_longer = true;
    }
    else
    {
      if(argc == i+1)
      {//if this is last param => file name
        if((params.file = fopen(argv[i], "r")) == NULL)
        {
          fprintf(stderr, "Error, can't read a file\n");
          exit(1);
        }
      }
      else
      {//some shit found on cmd
        fprintf(stderr, "Error, Wrong argument found\n");
        exit(1);
      }
    }
  }

  return params;
}

/**
 * Formating input text to specific output printed to stdout
 * @param params Struct with parameters of program
 * @return Zero if OK, none zero if error
 */
int format_text(Params *params)
{
  char word[4096] = {0}; //buffer for words
  char c = 0;
  unsigned long letters = 0; //letters in actual word
  unsigned long line = 0; //lenght of actually processed line
  bool reported_line_overflow = false; //report of cutting word
  bool first_empty_printed = false;
  bool reported_buffer_overflow = false;
  bool first_enter = false;
  
  while((c = getc(params->file)) != EOF)
  {
    if(isspace(c)) //end of word
    {

      if(letters) //first white space, flush buffer
      {
        if(c == '\n') // empty lines detector
          first_enter = true;

        word[letters] = '\0'; //delimmiter of string
        //must not be equal because of white char before word
        if(line + letters < params->line_width)
        {
          if(line)
          { //dont add space in begin of line
            putchar(' ');
            line++;
          } 
          printf("%s", word);
          line += letters;
        }
        else
        {
          if(params->cut_longer == true && letters > params->line_width)
          {
            if(reported_line_overflow == false)
            { //Report of error just once
              reported_line_overflow = true;
              fprintf(stderr, "Warning, some words are longer than line\n");
            }
            word[params->line_width] = '\0';
          }
          printf("\n%s", word);
          line = letters;
        }
        letters = 0;
      }
      else if(c == '\n')
      { //printing empty lines
        if(first_enter == true)
        {
	  if(first_empty_printed == false)
          {
	    putchar('\n');
	    first_empty_printed = true;
          }
          putchar('\n');
        }
	else
	  first_enter = true;
      }
      continue;
    }
    
    if(first_empty_printed == true)
    {
      first_empty_printed = false;
      line = 0;
      letters = 0;
    }
    
    first_enter = false;

    if(letters < 4095) //4095 (4096-1) because of '\0' at end of string word[4095]
    { //buffer overflow protection
      word[letters++] = c;
    }
    else if(reported_buffer_overflow == false)
    {  
      fprintf(stderr, "Warning, too long word reached end of buffer, word cut\n");
      reported_buffer_overflow = true;
    }
      
  }
  //putchar('\n');
  return 0;
}

int main(int argc, char **argv){
  
  Params params = get_params(argc, argv);
  format_text(&params);
  fclose(params.file);

  return EXIT_SUCCESS;
}
