//////////////////////////////////
// file:     htab_statistics.c
// project:  IJC - DU2
// compiler: GCC 4.8.1
// created:  2014-04-17
// finish:   2014-??-??
//
// author:  Pavel Mencner
//          University of technology Brno - FIT
//
// Description: Print statistics of hash table lists
// 
//////////////////////////////////

#include <limits.h>
#include <stdio.h>
#include "htable.h"

void htab_statistics(htab_t *t)
{
  htab_listitem_t *item_ptr = NULL;
  unsigned counter = 0;
  unsigned max = 0; //so first list should be bigger
  unsigned min = UINT_MAX; //so first list should be smaller
  unsigned long sum = 0;

  for(unsigned i = 0; i < t->htab_size; i++)
  {
    item_ptr = t->ptr[i];
    counter = 0;

    while(item_ptr != NULL)
    {
      counter++;
      item_ptr = item_ptr->next;
    }

    if(counter > max)
      max = counter;
    if(counter < min)
      min = counter;
    
    sum += counter;
  }

  printf("=====Statistics of hash table:=====\n");
  printf("Hash table size: %u\n", t->htab_size);
  printf("Total sum of items: %lu\n", sum);
  printf("Minimal length of list: %u\n", min);
  printf("Maximal length of list: %u\n", max);
  printf("Avarage length of list: %lu\n", sum/(t->htab_size));
  printf("===================================\n");
}
