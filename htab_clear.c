//////////////////////////////////
// file:     htab_clear.c
// project:  IJC - DU2
// compiler: GCC 4.8.1
// created:  2014-04-17
// finish:   2014-04-18
//
// author:  Pavel Mencner
//          University of technology Brno - FIT
//
// Description: Clear (delete) all items in hash table
// 
//////////////////////////////////

#include "htable.h"

void htab_clear(htab_t *t)
{
  htab_listitem_t *item_ptr = NULL;
  htab_listitem_t *tmp_ptr = NULL;
  
  for(unsigned i = 0; i < t->htab_size; i++)
  {
    item_ptr = t->ptr[i];
    t->ptr[i] = NULL;

    while(item_ptr != NULL)
    { //free list
      tmp_ptr = item_ptr;
      item_ptr = item_ptr->next;
      free(tmp_ptr);
    }
  }
}
