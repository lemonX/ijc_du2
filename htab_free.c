//////////////////////////////////
// file:     htab_free.c
// project:  IJC - DU2
// compiler: GCC 4.8.1
// created:  2014-04-17
// finish:   2014-04-18
//
// author:  Pavel Mencner
//          University of technology Brno - FIT
//
// Description: Free all the hash table memory, within lists
// 
//////////////////////////////////

#include "htable.h"

void htab_free(htab_t *t)
{
  htab_clear(t); //clear lists
  free(t); //delete hash table
}
