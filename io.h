//////////////////////////////////
// file:     io.h
// project:  IJC - DU2
// compiler: GCC 4.8.1
// created:  2014-04-18
// finish:   2014-04-18
//
// author:  Pavel Mencner
//          University of technology Brno - FIT
//
// Description: Interface for io.c
// 
//////////////////////////////////

int fgetw(char *s, int max, FILE *f);
