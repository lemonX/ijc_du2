//////////////////////////////////
// file:     htab_foreach.c
// project:  IJC - DU2
// compiler: GCC 4.8.1
// created:  2014-??-??
// finish:   2014-??-??
//
// author:  Pavel Mencner
//          University of technology Brno - FIT
//
// Description: CallBack function, call function() for each item in hash table
// 
//////////////////////////////////

#include "htable.h"

void htab_foreach(htab_t *t, void (*function)(const char *key, unsigned long value))
{
  htab_listitem_t *item_ptr = NULL;

  for(unsigned i = 0; i < t->htab_size; i++)
  {
    item_ptr = t->ptr[i];

    while(item_ptr != NULL)
    {
      function(item_ptr->key, item_ptr->data);
      item_ptr = item_ptr->next;
    }
  }
}
