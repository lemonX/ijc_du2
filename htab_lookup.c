//////////////////////////////////
// file:     htab_lookup.c
// project:  IJC - DU2
// compiler: GCC 4.8.1
// created:  2014-04-16
// finish:   2014-04-18
//
// author:  Pavel Mencner
//          University of technology Brno - FIT
//
// Description: Looking up for key in hash table if cant find it,
// will make it
// 
//////////////////////////////////

#include <string.h>
#include "htable.h"

struct htab_listitem *htab_lookup(htab_t *t, const char *key)
{
  unsigned hindex = hash_function(key, t->htab_size); //index to hash table
  htab_listitem_t *item_ptr = t->ptr[hindex]; //ptr to list
  htab_listitem_t **list_ptr = &(t->ptr[hindex]); //when we would make 
  
  while(item_ptr != NULL && strcmp(item_ptr->key, key) != 0)
    item_ptr = item_ptr->next;
  
  if(item_ptr == NULL)
  { //item not found, make it
    //malloc struct + key string + null byte
    item_ptr = (htab_listitem_t *) malloc(sizeof(htab_listitem_t) + strlen(key) + 1);
    
    if(item_ptr != NULL) //malloc successfull
    {
      item_ptr->next = *list_ptr;
      strcpy(item_ptr->key, key);
      item_ptr->data = 0;

      *list_ptr = item_ptr;
    }
  }
  
  return item_ptr;
}
